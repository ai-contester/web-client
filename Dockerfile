FROM    alpine:3.5

RUN     apk add --update --no-cache nodejs nginx

COPY    . /app/
RUN     cd /app \
        && npm config set strict-ssl false \
        && npm install \
        && npm run build \
        && apk del nodejs \
        && rm -rf /app/node_modules/* \
        && rm -rf /var/cache/apk/*

RUN     cp /app/nginx.conf /etc/nginx/nginx.conf

RUN     mkdir /run/nginx

EXPOSE  80

CMD     ["nginx", "-g", "daemon off;"]
