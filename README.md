# web-client
It is a web-client for AI-Contester server.

# Build and run
First of all you need to install NodeJS and NPM.

Then, run in project folder:
``` bash
npm install
```

## Run development server
``` bash
npm start
```
New tab will be opened in your browser.
Otherwise, look at console output.

## Build app for deployment
``` bash
npm run build
```
Then, set up any HTTP server to serve files from ```build``` directory.

# Configuration
You can set ip and port of backend server in ```public/config.js``` file.
