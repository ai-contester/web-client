import * as React from 'react';
import NicknameAutoComplete from './nickname-autocomplete';
import RaisedButton from 'material-ui/RaisedButton'
import request from './fetch-wrapper'
import Snackbar from 'material-ui/Snackbar'

export interface IAddSessionProps {
}

export interface IAddSessionState {
    opponents?: string[],
    successMessage?: boolean,
    errorMessage?: boolean
}

export class AddSession extends React.Component<IAddSessionProps, IAddSessionState> {
    constructor(props: IAddSessionProps) {
        super(props);
        this.state = {
            opponents: [], successMessage: false, errorMessage: false
        };
    }

    handleNickname = (nickname: string, opponentNumber: number) => {
        let opponents = this.state.opponents
        opponents[opponentNumber] = nickname
        this.setState({opponents: opponents})
    }

    handleSubmit = () => {
        request('sessions', 'post', this.state.opponents)
            .then(() => this.setState({successMessage: true}))
            .catch(() => this.setState({errorMessage: true}))
    }

    closeSuccessMessage = () => this.setState({successMessage: false})

    closeErrorMessage = () => this.setState({errorMessage: false})

    render() {
        return (
            <div>
                <form>
                    <NicknameAutoComplete
                        onNicknameChoosed={this.handleNickname}
                        opponentNumber={0}
                    /><br/>
                    <NicknameAutoComplete
                        onNicknameChoosed={this.handleNickname}
                        opponentNumber={1}
                    /><br/>
                    <NicknameAutoComplete
                        onNicknameChoosed={this.handleNickname}
                        opponentNumber={2}
                    />
                </form>
                <RaisedButton
                    label='Create session'
                    onClick={ this.handleSubmit }
                />
                <Snackbar
                    message='Session has been started'
                    open={this.state.successMessage}
                    onRequestClose={this.closeSuccessMessage}
                    autoHideDuration={2000}
                />
                <Snackbar
                    message='Error of session starting. Try later.'
                    open={this.state.errorMessage}
                    onRequestClose={this.closeErrorMessage}
                    onActionTouchTap={this.closeErrorMessage}
                    action='Close'
                />
            </div>
        );
    }
}
