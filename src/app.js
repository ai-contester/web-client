import * as React from 'react';
import { Link } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {List, ListItem} from 'material-ui/List'
import AppBar from 'material-ui/AppBar'
import Person from 'material-ui/svg-icons/social/person'
import FileUpload from 'material-ui/svg-icons/file/file-upload'
import Storage from 'material-ui/svg-icons/device/storage'
import PlaylistPlay from 'material-ui/svg-icons/av/playlist-play'
import People from 'material-ui/svg-icons/social/people'
import {IUser} from './IUser'
import Close from 'material-ui/svg-icons/navigation/close'
import Login from './login';
import Signup from './signup';
import {Tab, Tabs} from 'material-ui/Tabs'
import Add from 'material-ui/svg-icons/content/add'

var endpoint = "http://" + window.config.backend.ip + ":" + window.config.backend.port + "/";

interface IAppProps {
    children: any
}

interface IAppState {
    accessToken: string,
    me: IUser,
    authTab: string
}

export default class App extends React.Component<IAppProps, IAppState> {
    constructor(props: IAppProps) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        var token = localStorage.getItem("token");
        if (token) {
            fetch(endpoint + 'me/', {
                method: 'get',
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            }).then(res => {
                if (res.status === 200) {
                    res.json().then(data => {
                        this.setState({ me: data });
                    });
                }
                else if (res.status === 401) {
                    localStorage.removeItem("token");
                    return;
                }
                else {
                    console.log("Unexpected code: " + res.status);
                }
            })
            .catch(ex => {
                console.log('parsing failed', ex);
            });

        }
    }

    handleLogin = () => {
        var token = localStorage.getItem('token');
        fetch(endpoint + 'me/', {
            method: 'get',
            headers: {
                'Authorization': 'Bearer ' + token
            }
        }).then(res => {
            if (res.status === 200) {
                res.json().then(data => {
                    this.setState({ me: data });
                });
            }
            else if (res.status === 401) {
                localStorage.removeItem("token");
                return;
            }
            else {
                console.log("Unexpected code: " + res.status);
            }
        })
        .catch(ex => {
            console.log('parsing failed', ex);
        });
    }

    handleSignup = () => {
        this.setState({ authTab: 'login' })
    }

    handleTabsChange = (value) => {
        if (typeof(value) !== 'string' ) {
            return;
        }
        this.setState({ authTab: value })
    }

    logout(){
        this.setState({me: null });
        localStorage.removeItem("token");
    }

    updateUser(user) {
        console.log(this.state);
        this.setState({me: user});
    }

    childrenWithProps() {
        return this.props.children && React.cloneElement(this.props.children,
            {
                'me': this.state.me,
                'updateUser': (user) => this.updateUser(user)
            });
    }

    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <AppBar
                        showMenuIconButton={false}
                        title='AI-Contester'
                    />
                    {this.state.me ?
                        <div
                            style={{
                                display: 'flex',
                                flexDirection: 'row'
                            }}
                        >
                            <div>
                                <List>
                                    <Link to='/profile'>
                                        <ListItem
                                            primaryText='My profile'
                                            leftIcon={<Person/>}
                                        />
                                    </Link>
                                    <Link to='/strategies/new'>
                                        <ListItem
                                            primaryText='Send strategy'
                                            leftIcon={<FileUpload/>}
                                        />
                                    </Link>
                                    <Link to='/strategies'>
                                        <ListItem
                                            primaryText='My strategies'
                                            leftIcon={<Storage/>}
                                        />
                                    </Link>
                                    <Link to='/sessions/new'>
                                        <ListItem
                                            primaryText='New session'
                                            leftIcon={<Add/>}
                                        />
                                    </Link>
                                    <Link to='/sessions'>
                                        <ListItem
                                            primaryText='Sessions'
                                            leftIcon={<PlaylistPlay/>}
                                        />
                                    </Link>
                                    <Link to='/users'>
                                        <ListItem
                                            primaryText='Сompetitors'
                                            leftIcon={<People/>}
                                        />
                                    </Link>
                                    <ListItem
                                        primaryText='Logout'
                                        leftIcon={<Close/>}
                                        onTouchTap={()=>this.logout()}
                                    />
                                </List>
                            </div>
                            <div>
                                {this.childrenWithProps()}
                            </div>
                        </div>
                        :
                        <Tabs
                            value={this.state.authTab}
                            onChange={this.handleTabsChange}
                        >
                            <Tab
                                label='log in'
                                value='login'
                            >
                                <Login onLogin={this.handleLogin}/>
                            </Tab>
                            <Tab
                                label='sign up'
                                value='signup'
                            >
                                <Signup onSignup={ this.handleSignup }/>
                            </Tab>
                        </Tabs>
                    }
                </div>
            </MuiThemeProvider>
        );
    }
}
