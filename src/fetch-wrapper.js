var endpoint = "http://" + window.config.backend.ip + ":" + window.config.backend.port + "/";

interface RequestOptions {
    method: String,

}

export default function request(url: string, method: string, data: Object) {
    var token = localStorage.getItem('token');
    var options: RequestInit = {
        method: method,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    };
    if(method.toLowerCase() !== 'get') {
        options.body = JSON.stringify(data);
    }
    return fetch(endpoint + url, options).then(response => {
        if(response.status===400) {
            console.log(response.status, response.statusText)
            response.json().then(data=>{
                console.log(data.message)
            });
            console.error('Bad request')
            throw new Error('Bad request')
        } else if (response.status === 401) {
            localStorage.removeItem('token');
            console.error('Unauthorized')
            throw new Error('Unauthorized')
        } else if (response.status === 405) {
            console.error('Method not allowed')
            throw new Error('Method not allowed')
        } else if (response.status === 404) {
            console.error('Not found')
            throw new Error('Not found')
        }

        return response.json().then(data => {
           data.httpStatus = response.status
           return data
        });
    })
}
