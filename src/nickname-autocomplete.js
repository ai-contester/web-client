import * as React from 'react';
import AutoComplete from 'material-ui/AutoComplete';
import request from './fetch-wrapper'

interface INicknameAutoCompleteProps {
    onNicknameChoosed(string, number): void;
    opponentNumber: number;
}

interface INicknameAutoCompleteState {
    names?: Array<string>;
    nicknameChoosed?: boolean;
}

export default class AddSession extends React.Component<INicknameAutoCompleteProps, INicknameAutoCompleteState> {
    constructor(props: INicknameAutoCompleteProps) {
        super(props);
        this.state = {names: [], nicknameChoosed: false};
    }

    processNicknameChoice = (nickname: string, index: number) => {
        if (nickname === this.state.names[index]) {
            this.props.onNicknameChoosed(nickname, this.props.opponentNumber)
        } else {
            this.props.onNicknameChoosed(null, this.props.opponentNumber)
        }
    }

    handleNicknameInput = (nickname: string) => {
        request('users?username-starts-with='+nickname + '&has-strategy=true', 
                'get', null).then(data => {
            this.setState({
                names: data.map(user => user.username),
                nicknameChoosed: nickname === this.state.names[0]
            }, () => this.processNicknameChoice(nickname, 0));

        });
    }

    handleNewRequest = (nickname: string, index: number) => {
        if (index === -1) {
            index = 0
        }
        this.setState({
            nicknameChoosed: nickname === this.state.names[index]
        }, () => this.processNicknameChoice(nickname, index))
    }

    render() {
        let numbers = [
            "First", "Second", "Third"
        ]
        let floatingText = numbers[this.props.opponentNumber] + ' opponent'
        return (
            <div>
                <AutoComplete
                    dataSource={this.state.names}
                    hintText='Type nickname'
                    floatingLabelText={floatingText}
                    onUpdateInput={this.handleNicknameInput}
                    onNewRequest={this.handleNewRequest}
                />
                <span>
                    {this.state.nicknameChoosed ? "OK!" : null}
                </span>
            </div>
        );
    }
}
