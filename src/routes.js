import * as React from 'react';
import { Router, Route, useRouterHistory } from 'react-router';
import { createHashHistory } from 'history'
import {SendStrategy} from './send-strategy';
import {StrategiesList} from './strategies-list';
import {SessionsList} from './sessions-list'
import App from './app'
import Profile from './profile'
import {UsersList} from './users-list'
import {AddSession} from './add-session'


export default (
  <Router history={useRouterHistory(createHashHistory)({queryKey: false})}>
    <Route path="/" component={App}>
      <Route path="profile" component={Profile}/>
      <Route path="strategies/new" component={SendStrategy}/>
      <Route path="strategies" component={StrategiesList}/>
      <Route path="sessions/new" component={AddSession}/>
      <Route path="sessions" component={SessionsList}/>
      <Route path="users" component={UsersList}/>
    </Route>
  </Router>
)
