import * as React from 'react';
import {TableRow, TableRowColumn} from 'material-ui/Table'

interface ISessionRowProps {
    session: any
}

interface ISessionRowState {
}

export class SessionRow extends React.Component<ISessionRowProps, ISessionRowState> {
    constructor(props: ISessionRowProps) {
        super(props);
        this.state = {errorDialogOpened: false};
    }

    componentDidMount(){
    }

    // POST doesn't save type of date and JS think, that it's string!
    // TODO: Need to solve this later!
    formatDate(rawDate: any) {
        var d = new Date(rawDate);
        return d.getHours() + ":" + d.getMinutes() + " "
            + d.getDate() + "." + d.getMonth() + "." + d.getFullYear();
    }

    render() {
        return (
             <TableRow>
                <TableRowColumn>
                    {this.formatDate(this.props.session.date)}
                </TableRowColumn>
                <TableRowColumn>
                    { this.props.session.status }
                </TableRowColumn>
                <TableRowColumn>
                    <a
                        href={"data:text/plain;charset=utf-8," +
                            encodeURIComponent(this.props.session.gameStory) }
                        target="_blank"
                        download={"log-" + this.props.session.id + ".json"}
                    >
                        Download log
                    </a>
                </TableRowColumn>
            </TableRow>
        );
    }
}
