import * as React from 'react';
import request from './fetch-wrapper'
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow} from 'material-ui/Table'
import {SessionRow} from './session-row'

export interface ISessionsListProps {
}

export interface ISessionsListState {
  sessions: Any[]
}

export class SessionsList extends React.Component<ISessionsListProps, ISessionsListState> {
  constructor(props: ISessionsListProps) {
      super(props);
      this.state = { sessions: [] };
  }

  componentDidMount(){
      this.refresh();
  }

  refresh() {
      request('sessions', 'get', null).then(data => {
          this.setState({ sessions: data });
      });
  }

  // POST doesn't save type of date and JS think, that it's string!
  // TODO: Need to solve this later!
  formatDate(rawDate: any) {
      var d = new Date(rawDate);
      return d.getHours() + ":" + d.getMinutes() + " "
          + d.getDate() + "." + d.getMonth() + "." + d.getFullYear();
  }

  render() {
      var sessions = this.state.sessions.map(((session) =>
          <SessionRow session={session} key={session.id}/>
      ));
      return (
          <div>
              <h3>There are ({this.state.sessions.length}) sessions</h3>
              <button
                  onClick={() => this.refresh() }
                  > Refresh </button>
              <Table>
                  <TableHeader
                      displaySelectAll={false}
                      adjustForCheckbox={false}
                  >
                      <TableRow>
                          <TableHeaderColumn>Date</TableHeaderColumn>
                          <TableHeaderColumn>Status</TableHeaderColumn>
                          <TableHeaderColumn>Log</TableHeaderColumn>
                      </TableRow>
                  </TableHeader>
                  <TableBody displayRowCheckbox={false}>
                      {sessions}
                  </TableBody>
              </Table>
          </div>
      );
  }
}
