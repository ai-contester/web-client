import * as React from 'react';
import {IStrategy} from './IStrategy'
import {Table, TableHeader, TableHeaderColumn, TableBody, TableRow} from 'material-ui/Table'
import {StrategyRow} from './strategy-row'
import request from './fetch-wrapper'

interface IStrategiesListProps {
}

interface IStrategiesListState {
    strategies?: IStrategy[];
}

export class StrategiesList extends React.Component<IStrategiesListProps, IStrategiesListState> {
    constructor(props: IStrategiesListProps) {
        super(props);
        this.state = { strategies: [] };
    }

    componentDidMount(){
        this.refresh();
    }

    refresh() {
        request('strategies', 'get', null).then(data => {
            this.setState({ strategies: data });
        });
    }

    // POST doesn't save type of date and JS think, that it's string!
    // TODO: Need to solve this later!
    formatDate(rawDate: any) {
        var d = new Date(rawDate);
        return d.getHours() + ":" + d.getMinutes() + " "
            + d.getDate() + "." + d.getMonth() + "." + d.getFullYear();
    }

    render() {
        var strategies = this.state.strategies.map(((strategy) =>
            <StrategyRow strategy={strategy} key={strategy.id}/>
        ));
        return (
            <div>
                <h3>There are ({this.state.strategies.length}) strategies</h3>
                <button
                    onClick={() => this.refresh() }
                    > Refresh </button>
                <Table>
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn>Date</TableHeaderColumn>
                            <TableHeaderColumn>Status</TableHeaderColumn>
                            <TableHeaderColumn>Source</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {strategies}
                    </TableBody>
                </Table>
            </div>
        );
    }
}
