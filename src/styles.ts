export const styles = {
    header: {
        textAlign: 'center'
    },
    menu: {
        float: 'left'
    },
    content: {
        overflow: 'hidden',
        paddingLeft: '20px'
    },
    activeLink: {

    }
};
